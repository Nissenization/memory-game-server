const WebSocket = require('ws');
const memory = require('./src/memory');
const port = 3000;

const wss = new WebSocket.Server({ port });

wss.on('connection', function connection(ws) {
  ws.on('message', async (message) => {
    const { command, context } = JSON.parse(message);
    try {
      switch (command) {
        case 'createGame':
          return await createGame(context);
        case 'joinGame':
          return await joinGame(context);
        case 'startGame':
          return await startGame(context);
        case 'turnCards':
          return await turnCards(context);
        case 'revertToMove':
          return await revertToMove(context);
        default:
          ws.send(JSON.stringify({ event: 'error', context: { message: 'command not handled' } }));
      }
    } catch(err) {
      console.dir(err, { colors: true, depth: 4 });
      ws.send(JSON.stringify({ event: 'error', context: { message: err.message } }));
    }
  });
});

async function joinGame(context) {
  const { id, playerId } = context;
  await memory.joinGame(id, playerId);
  sendAll('playerJoined', { id, playerId });
}

async function createGame(context) {
  const { playerId } = context;
  const { id } = await memory.createGame(playerId);
  sendAll('gameCreated', { id, playerId });
}

async function startGame(context) {
  const { id, playerId } = context;
  await memory.startGame(id);
  const { cards, nextTurn } = await memory.getGame(id);
  sendAll('gameStarted', { id, cards, nextTurn });
}

async function revertToMove(context) {
  const { id, moveId } = context;
  await memory.revertToMove(id, moveId);

  const game = await memory.getGame(id);

  const { cards, nextTurn } = game;
  sendAll('gameStarted', { id, cards, nextTurn });
}

async function turnCards(context) {
  const { id, playerId, card1, card2 } = context;
  const event = await memory.turnCards(id, playerId, card1, card2);
  sendAll(event.name, event.context);

  const { state, players, moves } = await memory.getGame(id);
  if (state === 'ended') {
    sendAll('gameEnded', { id, players, moves });
  }
}

function sendAll(event, context) {
  wss.clients.forEach(function each(client) {
    client.send(JSON.stringify({ event, context }));
  });
}
