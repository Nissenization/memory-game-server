const chai = require('chai');
chai.use(require('chai-as-promised'));
const expect = chai.expect;
const memory = require('../src/memory');
const eventStore = require('../src/eventstore');

describe('Memory Game', () => {

  beforeEach(() => {
    eventStore.reset();
  });

  describe('#createGame', () => {

    it('should create game and wait for players to enter', () => {
      const { id } = memory.createGame('player-one');
      expect(id).to.not.equal(null);
    });

    it('should make unique game ids when creating more games', async () => {
      const game1 = await memory.createGame('player-one');
      const game2 = await memory.createGame('player-one');
      expect(game1.id).to.not.equal(game2.id);
    });
  });

  describe('#startGame', () => {

    it('should start game with two players', async () => {
      const { id } = await memory.createGame('player-one');
      await memory.joinGame(id, 'player-two');
      await memory.startGame(id);

      const game = await memory.getGame(id);
      expect(game.players).to.eql([ { id: 'player-one', score: 0 }, { id: 'player-two', score: 0 } ]);
      expect(game.state).to.equal('started');
    });

  });

  describe('#turnCards', () => {

    it('should end game after one draw with 2 cards', async () => {
      const { id } = await memory.createGame('player-one', 2);
      await memory.joinGame(id, 'player-two');
      await memory.startGame(id);
      await memory.turnCards(id, 'player-one', 1, 2);

      const game = await memory.getGame(id);
      expect(game.state).to.equal('ended');
      expect(game.players).to.eql([
        { id: 'player-one', score: 1 },
        { id: 'player-two', score: 0 }
      ]);

      expect(game.moves[0].playerId).to.equal('player-one');
      expect(game.moves[0].card1).to.eql({ id: 1, value: 1, selected: true });
      expect(game.moves[0].card2).to.eql({ id: 2, value: 1, selected: true });
      expect(game.moves[0].matched).to.equal(true);
    });

    it('should throw error if player1 tries to turnCards 2 times in a row', async () => {
      const { id } = await memory.createGame('player-one');
      await memory.joinGame(id, 'player-two');
      await memory.startGame(id);
      await memory.turnCards(id, 'player-one', 1, 2);
      await expect( memory.turnCards(id, 'player-one', 3, 4)).to.eventually
        .be.rejectedWith('Its not your turn!');
    });

    it('should throw error if player2 tries to draw first', async () => {
      const { id } = await memory.createGame('player-one');
      await memory.joinGame(id, 'player-two');
      await memory.startGame(id);
      await expect( memory.turnCards(id, 'player-two', 3, 4)).to.eventually
        .be.rejectedWith('Its not your turn!');
    });
  });

  describe('#revertToMove', () => {

    it('should go back to selected move', async () => {
      const { id } = await memory.createGame('player-one');
      await memory.startGame(id);
      await memory.turnCards(id, 'player-one', 1, 2);
      await memory.turnCards(id, 'player-one', 1, 3);
      await memory.turnCards(id, 'player-one', 1, 4);
      await memory.turnCards(id, 'player-one', 1, 5);

      const gameBefore = await memory.getGame(id);
      expect(gameBefore.moves).to.have.length(4);

      const moveId = gameBefore.moves[2].moveId;
      await memory.revertToMove(id, moveId);

      const game = await memory.getGame(id);
      expect(game.moves).to.have.length(2);
      expect(game.moves[0].card2).to.eql(gameBefore.moves[0].card2);
      expect(game.moves[1].card2).to.eql(gameBefore.moves[1].card2);
    });

    it('should only have 2 cards selected when reverted to second successful move', async () => {
      const cardNumber = 4;

      const { id } = await memory.createGame('player-one', cardNumber);
      await memory.startGame(id);

      const moves = [];
      for (i = 1; i <= cardNumber; i++) {
        for (j = 1; j<= cardNumber; j++) {
          if (i === j) {
            continue;
          }

          if (i > j) {
            continue;
          }

          const move = await memory.turnCards(id, 'player-one', i, j);
          moves.push(move);
        }
      }

      const successFullMoves = moves.filter(move => move.context.matched);
      const secondMoveId = successFullMoves[1].context.moveId;

      await memory.revertToMove(id, secondMoveId);
      const game = await memory.getGame(id);

      const cards = game.cards.filter(card => card.selected);
      expect(cards).to.have.length(2);
    });

  });

  describe('#getGame', () => {
    it('should return a game just created', async () => {
      const { id } = await memory.createGame('player-one');
      const game = await memory.getGame(id);
      expect(game.id).to.equal(id);
      expect(game.state).to.equal('open');
      expect(game.cards).to.have.length(18);
    });

    it('should return pending state if game is still ongoing', async () => {
      const { id } = await memory.createGame('player-one');
      await memory.joinGame(id, 'player-two');
      await memory.startGame(id);
      await memory.turnCards(id, 'player-one', 1, 2);

      const game = await memory.getGame(id);
      expect(game.state).to.equal('started');
    });
  });

  describe('#getOpenGames', () => {

    it('should return empty list if there are no open game', async () => {
      const games = await memory.getOpenGames();
      expect(games).to.eql([]);
    });

    it('should return a game that was just created', async () => {
      const { id } = await memory.createGame('player-one');
      const game = await memory.getGame(id);
      const games = await memory.getOpenGames();
      expect(games).to.eql([ game ]);
    });

    it('should return list of more games that was just created', async () => {
      const { id: id1 } = await memory.createGame('player-one');
      const { id: id2 } = await memory.createGame('player-one');
      const { id: id3 } = await memory.createGame('player-one');
      const games = await memory.getOpenGames();
      expect(games).to.have.length(3);
      const [ game1, game2, game3 ] = games;
      expect(game1.id).to.equal(id1);
      expect(game2.id).to.equal(id2);
      expect(game3.id).to.equal(id3);
    });

  });

});
