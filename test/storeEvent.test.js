const expect = require('chai').expect;
const eventStore = require('../src/eventstore');

describe('EventStore', () => {

  it('should not be able to mutate event store events', async () => {
    const aggregateId = 1;
    await eventStore.storeEvent({
      aggregateId,
      context: {
        value: 1234
      }
    });

    const events = await eventStore.getEvents(aggregateId);

    // Mutate events
    for (const event of events) {
      event.context.value = 1337;
    }

    const eventsAfter = await eventStore.getEvents(aggregateId);

    expect(eventsAfter).to.eql([ {
      aggregateId,
      context: {
        value: 1234
      }
    } ]);
  });
});
