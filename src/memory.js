const uuid = require('uuid');
const eventStore = require('./eventstore');
const Memory = require('./domain/Memory');

module.exports = {
  createGame,
  joinGame,
  startGame,
  turnCards,
  getGame,
  getOpenGames,
  revertToMove
};

async function createGame(playerId, cards) {
  const { id, gameCreatedEvent } = Memory.createGame(playerId, cards);
  await eventStore.storeEvent(gameCreatedEvent);
  return { id };
}

async function joinGame(id, playerId) {
  const game = await getGame(id);
  const playedJoinedEvent = game.joinGame(playerId);
  await eventStore.storeEvent(playedJoinedEvent);
}

async function startGame(id) {
  const game = await getGame(id);
  const gameStartedEvent = game.startGame();
  await eventStore.storeEvent(gameStartedEvent);
}

async function turnCards(id, player, card1, card2) {
  const game = await getGame(id);
  const event = game.turnCards(player, card1, card2);
  await eventStore.storeEvent(event);
  return event;
}

async function getGame(id) {
  const events = await eventStore.getEvents(id);
  return Memory.applyEvents(events);
}

async function getOpenGames() {
  const gamesWithEvents = await eventStore.getAllEvents();
  return gamesWithEvents.map(events => Memory.applyEvents(events));
}

async function revertToMove(id, moveId) {
  await eventStore.revertEvents(id, moveId);
}
