const _ = require('lodash');

module.exports = {
  storeEvent,
  getEvents,
  getAllEvents,
  revertEvents,
  reset
};

const store = new Map();

async function storeEvent(event) {
  const { aggregateId } = event;
  if (!store.has(aggregateId)) {
    store.set(aggregateId, [ event ]);
  } else {
    const events = store.get(aggregateId);
    events.push(event);
    store.set(aggregateId, events);
  }
}

async function getEvents(aggregateId) {
  const events = store.get(aggregateId);

  // clone event object to avoid mutating
  return _.cloneDeep(events);
}

async function getAllEvents() {
  return [ ...store.values() ];
}

async function revertEvents(aggregateId, eventId) {
  const events = store.get(aggregateId);
  // Find index of event and remove all events after
  const eventIndex = events.findIndex(event => event.eventId === eventId);

  store.set(aggregateId, events.slice(0, eventIndex));
}

async function reset() {
  store.clear();
}
