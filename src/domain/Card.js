class Card {
  constructor(id, value, selected = false) {
    this.id = id;
    this.value = value;
    this.selected = selected;
  }

  static createCardPairs(numberOfCards) {
    const randomValues = [ ...Array(numberOfCards).keys() ]
      .map(x => Math.round((x+1)/2))
      .sort(() => Math.random() - 0.5);

    const cards = [];
    for (let i = 1; i<=numberOfCards; i++) {
      cards.push(new Card(i, randomValues.pop()));
    }

    return cards;
  }

  matches(otherCard) {
    return this.value === otherCard.value;
  }
}

module.exports = Card;
