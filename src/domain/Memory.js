const uuid = require('uuid');
const Card = require('./Card');

const DEFAULT_CARDS = 18;

const STATE_OPEN = 'open';
const STATE_STARTED = 'started';
const STATE_ENDED = 'ended';

const GAME_CREATED_EVENT = 'gameCreated';
const PLAYER_JOINED_EVENT = 'playerJoined';
const GAME_STARTED_EVENT = 'gameStarted';
const CARDS_TURNED = 'cardsTurned';

class Memory {

  constructor({ id, cards, players, state, nextTurn, moves }) {
    this.id = id;
    this.cards = cards;
    this.players = players;
    this.state = state;
    this.nextTurn = nextTurn;
    this.moves = moves;
  }

  static createGame(playerId, numberOfCards = DEFAULT_CARDS) {
    const id = uuid.v4();

    const cards = Card.createCardPairs(numberOfCards);
    const gameCreatedEvent = {
      eventId: uuid.v4(),
      aggregateId: id,
      name: GAME_CREATED_EVENT,
      context: {
        cards, player: playerId
      }
    };

    return { id, gameCreatedEvent };
  }

  static applyEvents(events) {
    const gameAttributes = events.reduce((game, { eventId, context, name, aggregateId }) => {
      switch (name) {
        case GAME_CREATED_EVENT:
          game.id = aggregateId;
          game.cards = context.cards;
          game.players= [ { id: context.player, score: 0 } ];
          game.state = STATE_OPEN;
          break;
        case PLAYER_JOINED_EVENT:
          game.players = [ ...game.players, { id: context.player, score: 0 } ];
          break;
        case GAME_STARTED_EVENT:
          game.state = STATE_STARTED;
          game.nextTurn = context.nextTurn;
          game.moves = [];
          break;
        case CARDS_TURNED:
          if (context.matched) {
            for (const player of game.players) {
              player.score += (player.id === context.playerId) ? 1 : 0;
            }

            for (const card of game.cards) {
              if ([ context.card1.id, context.card2.id ].includes(card.id)) {
                card.selected = true;
              }
            }

            // Check if game is over
            if (game.cards.filter(card => !card.selected).length === 0) {
              game.state = STATE_ENDED;
            }
          }

          game.moves.push({
            moveId: eventId,
            playerId: context.playerId,
            card1: context.card1,
            card2: context.card2,
            matched: context.matched
          });

          game.nextTurn = context.nextTurn;
          break;
        default:
          throw new Error('Event not handled');
      }

      return game;
    }, {});

    return new Memory(gameAttributes);
  }

  startGame() {
    // Could have been random
    const nextTurn = this.players[0].id;
    return this.createEvent(GAME_STARTED_EVENT, { nextTurn });
  }

  joinGame(playerId) {
    return this.createEvent(PLAYER_JOINED_EVENT, { player: playerId });
  }

  turnCards(playerId, card1Id, card2Id) {
    if (this.nextTurn !== playerId) {
      throw new Error('Its not your turn!');
    }

    const index = this.players.findIndex(player => player.id === playerId);
    const card1 = this.cards.find(card => card.id === card1Id);
    const card2 = this.cards.find(card => card.id === card2Id);
    const matched = card1.matches(card2);

    if (matched) {
      card1.selected = true;
      card2.selected = true;
    }

    const nextTurn = matched ? playerId : this.players[((index + 1) % this.players.length)].id;

    const eventId = uuid.v4();
    return {
      eventId,
      aggregateId: this.id,
      name: CARDS_TURNED,
      context: { moveId: eventId, playerId, card1, card2, nextTurn, matched }
    };
  }

  createEvent(name, context) {
    return {
      eventId: uuid.v4(),
      aggregateId: this.id,
      name, context
    };
  }
}

module.exports = Memory;
